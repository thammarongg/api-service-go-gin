package controller

import (
	"tutorial/api-service-go-gin/dto"
	"tutorial/api-service-go-gin/service"

	"github.com/gin-gonic/gin"
)

type LoginController interface {
	Login(c *gin.Context) string
}

type loginController struct {
	loginService service.LoginService
	jwtService   service.JWTService
}

func LoginHandler(loginService service.LoginService, jwtService service.JWTService) LoginController {
	return &loginController{
		loginService: loginService,
		jwtService:   jwtService,
	}
}

func (controller *loginController) Login(c *gin.Context) string {
	var credentials dto.LoginCredential
	if c.ShouldBindJSON(&credentials) != nil {
		c.JSON(400, gin.H{"error": "Bad Request Body"})
		return ""
	}

	isUserAuthenticated := controller.loginService.LoginUser(credentials.Email, credentials.Password)
	if isUserAuthenticated {
		token := controller.jwtService.GenerateToken(credentials.Email, true)
		c.JSON(200, gin.H{
			"token": token,
		})
	} else {
		c.JSON(401, gin.H{
			"message": "Invalid credentials",
		})
	}

	return ""
}
