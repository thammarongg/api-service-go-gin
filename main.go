package main

import (
	"net/http"
	"tutorial/api-service-go-gin/controller"
	"tutorial/api-service-go-gin/middleware"
	"tutorial/api-service-go-gin/service"

	"github.com/gin-gonic/gin"
)

type album struct {
	Title  string `json:"title"`
	Artist string `json:"artist"`
	Year   string `json:"year"`
	ID     int    `json:"id"`
}

var albums = []album{
	{"Go Ahead", "Alicia Keys", "2000", 1},
	{"As I Am", "Alicia Keys", "2007", 2},
}

func getAlbums(c *gin.Context) {
	c.JSON(http.StatusOK, albums)
}

func main() {
	var loginService service.LoginService = service.StaticLoginService()
	var jwtService service.JWTService = service.JWTAuthService()
	var loginController controller.LoginController = controller.LoginHandler(loginService, jwtService)

	router := gin.Default()

	router.POST("/login", func(c *gin.Context) {
		token := loginController.Login(c)
		if token != "" {
			c.JSON(http.StatusOK, gin.H{"token": token})
			return
		} else {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "***Invalid credentials"})
			return
		}
	})

	v1 := router.Group("/v1")
	v1.Use(middleware.AuthorizeJWT())
	{
		v1.GET("/albums", getAlbums)
	}
	router.Run("localhost:8080")
}
