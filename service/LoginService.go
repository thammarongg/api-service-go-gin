package service

type LoginService interface {
	LoginUser(email string, password string) bool
}

type loginInformation struct {
	email    string
	password string
}

func StaticLoginService() LoginService {
	return &loginInformation{
		email:    "test@siamintech.co.th",
		password: "test",
	}
}

func (info *loginInformation) LoginUser(email string, password string) bool {
	if email == info.email && password == info.password {
		return true
	}

	return false
}
