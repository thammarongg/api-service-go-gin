package middleware

import (
	"fmt"
	"net/http"
	"tutorial/api-service-go-gin/service"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func AuthorizeJWT() gin.HandlerFunc {
	return func(c *gin.Context) {
		const BEARER_SCHEMA = "Bearer "
		authHeader := c.GetHeader("Authorization")
		if authHeader == "" {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Authorization header not found"})
			c.Abort()
			return
		} else if len(authHeader) < len(BEARER_SCHEMA) {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Authorization header malformed"})
			c.Abort()
			return
		} else if authHeader[:len(BEARER_SCHEMA)] != BEARER_SCHEMA {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Authorization header"})
			c.Abort()
			return
		} else {
			tokenString := authHeader[len(BEARER_SCHEMA):]
			token, err := service.JWTAuthService().ValidateToken(tokenString)
			if token.Valid {
				claims := token.Claims.(jwt.MapClaims)
				fmt.Println(claims)
			} else {
				fmt.Println("testing")
				fmt.Println(err)
				c.AbortWithStatus(http.StatusUnauthorized)
			}
		}
	}
}
